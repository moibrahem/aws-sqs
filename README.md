## Java Spring template project with amazon sqs Using a Docker image
## To run the project please follow the instructions below


### 1. Install [Docker](https://desktop.docker.com/mac/main/arm64/Docker.dmg)
In case you do have Homebrew

```sh
brew cask install docker
```
### 2. pull the  SQS image directly from Docker Hub
```sh
docker pull roribio16/alpine-sqs
```

### 3. Running the environment
```sh
docker run --name alpine-sqs -p 9324:9324 -p 9325:9325 -d roribio16/alpine-sqs:latest
```

### 4. Running the Spring boot project
Navigate to the root of the project via command line and execute the command

```sh
mvn spring-boot:run
```

### 5. Running the postman collection

```sh
resources/postmanCollection/sqs.postman_collection.json
```

## Resources
[Amazon SQS developer guide ](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/welcome.html)

