package com.aws.sqs.controller;

import com.amazonaws.services.sqs.model.SendMessageResult;
import com.aws.sqs.config.QueueConfiguration;
import com.aws.sqs.model.MessageObject;
import com.aws.sqs.service.QueueService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/queue")
public class QueueController {

    private final QueueConfiguration queueConfiguration;

    private final QueueService queueService;

    @PostMapping(path = "/default", produces = MediaType.APPLICATION_JSON_VALUE)
    public SendMessageResult sendMessageToFirstQueue(@RequestBody MessageObject message) throws JsonProcessingException {
        return queueService.sendSqsMessage(queueConfiguration.getDefaultQueue(), message);
    }


}
