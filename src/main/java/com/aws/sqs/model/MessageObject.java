package com.aws.sqs.model;

import lombok.Data;

@Data
public class MessageObject {
    private String name;
    private String title;
    private int amount;
    private String reason;

}
