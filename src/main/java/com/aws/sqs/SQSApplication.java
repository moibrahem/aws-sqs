package com.aws.sqs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SQSApplication {
    public static void main(String[] args) {
        SpringApplication.run(SQSApplication.class, args);
    }
}